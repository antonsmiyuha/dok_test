<?php

/**
 * Class Config
 */
class Config
{
    protected $_config = [];

    /**
     * Parse config from string to array
     * @param $string
     * @return $this
     */
    public function set($string)
    {
        $rows = explode("\n", $string);
        foreach ($rows as $row) {
            $pieces = explode('=', $row);
            if (count($pieces) !== 2) continue;
            $this->addConfigItem(array_reverse(explode('.', $pieces[0])), $this->_config, $pieces[1]);
        }
        return $this;
    }

    /**
     * Add item recursively by namespace
     * @param $namespace
     * @param $lastElement
     * @param $value
     * @return $this
     */
    protected function addConfigItem($namespace, &$lastElement, $value)
    {
        $key = array_pop($namespace);
        if (!isset($lastElement[$key])) $lastElement[$key];

        if (!empty($namespace)) {
            $this->addConfigItem($namespace, $lastElement[$key], $value);
        } else {
            $lastElement[$key] = strpos($value, ',') ? explode(',', $value) : $value;
        }
        return $this;
    }

    /**
     * Get config value by namespace or all config
     * @param null $namespace
     * @return mixed
     */
    public function get($namespace = null)
    {
        if ($namespace) {
            $namespace = array_reverse(explode('.', $namespace));
            return $this->getConfigValue($namespace, $this->_config);
        } else {
            return $this->_config;
        }
    }

    /**
     * Get item value recursively by namespace
     * @param $namespace
     * @param $lastElement
     * @return null|mixed
     */
    protected function getConfigValue($namespace, &$lastElement)
    {
        $key = array_pop($namespace);
        if (!isset($lastElement[$key])) return null;

        if (!empty($namespace)) {
            return $this->getConfigValue($namespace, $lastElement[$key]);
        } else {
            return $lastElement[$key];
        }
    }
}

$stringConfig = 'html.highlight.string=red
html.highlight.comment=black
html.highlight.bg=yellow
html.tags.decli=a,br,hr
html.tags.strict=1
php.connections.num=#DD0000
php.connections.check=true
php.safe_mode=1';


$config = new Config();
echo '<pre>';
print_r($config->set($stringConfig)->get('html.highlight.bg'));
echo '</pre>';
